import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Refresher, reorderArray } from 'ionic-angular';

import { ANIMALES  } from '../../data/data.animales';
import { Animal } from '../../interfaces/animales.interfaces';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  animales:Animal[] = [];
  audio = new Audio();
  audioTiempo: any;
  ordenando:boolean = false;

  constructor(public navCtrl: NavController) {

    this.animales = ANIMALES.slice(0);

    this.animales[0];

  }

  reproducir(animal:Animal){

    this.pausar_audio(animal);

    if( animal.reproduciendo ){
      animal.reproduciendo = false;
      return;
    }
    
    console.log(animal);
    this.audio.src = animal.audio;

    this.audio.load();
    this.audio.play();

    animal.reproduciendo = true;

    setTimeout(()=> animal.reproduciendo = true, animal.duracion * 1000);
  }

  private pausar_audio(animalSelec:Animal){

    clearTimeout(this.audioTiempo);

      this.audio.pause();
      this.audio.currentTime = 0;

      for(let animal of this.animales){

        if(animal.nombre != animalSelec.nombre){

          animal.reproduciendo = false;
        }
      }
  }

  borrar_animal( idx:number){
    this.animales.splice(idx, 1);
  }

  recargarAnimales(refresher:Refresher){
    console.log('Se ha comenzado a cargar', refresher);

    setTimeout(() => {
      console.log('Se ha refrescado la pagina');
      this.animales = ANIMALES.slice(0);
      refresher.complete();
    }, 1500);
  }

  reordenar_animales(indices:any){

    console.log(indices);
    this.animales = reorderArray(this.animales, indices);
  }

}
