Project Ionic Sounds App
==========================

Description project
------------------------

This project was developed in a Ionic course where I worked with arrays

Getting Started
------------------------

* Download the installer for **Node.js 6** or greater.
* Install the ionic CLI globally: **npm install -g ionic**
* Clone this repository: git clone https://gitlab.com/mari578cano/app-sonidos-ionic.git
* **Run npm install** from the project root.
* **Run ionic serve** in a terminal from the project root.
* Profit.

Author
---------------------

Mariana Cano
